<?php 

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');


$sheep = new animal("shaun");
echo "Nama : " .$sheep->name."<br>"; // "shaun"
echo "Legs : ".$sheep->legs."<br>"; // 4
echo "Cold blooded : ".$sheep->cold_blooded."<br>"; // "no"

echo "<br>";

$kodok = new frog("buduk");
echo "Nama : " .$kodok->name."<br>";
echo "Legs : ".$kodok->legs."<br>";
echo "Cold blooded : ".$kodok->cold_blooded."<br>";
$kodok->jump() ; // "hop hop"

echo"<br> <br>";

$sungokong = new ape("kera sakti");
echo "Nama : " .$sungokong->name."<br>";
echo "Legs : ".$sungokong->legs."<br>";
echo "Cold blooded : ".$sungokong->cold_blooded."<br>";
$sungokong->yell(); // "Auooo"

?>